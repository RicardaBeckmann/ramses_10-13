!################################################################
!################################################################
!################################################################
!################################################################
subroutine star_formation(ilevel)
  use amr_commons
  use pm_commons
  use hydro_commons
  use poisson_commons
  use cooling_module, ONLY: XH=>X, rhoc, mH, twopi 
  use random
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
#endif
  integer::ilevel
  !----------------------------------------------------------------------
  ! Description: This subroutine spawns star-particle of constant mass
  ! using a Poisson probability law if some gas condition are fulfilled. 
  ! It modifies hydrodynamic variables according to mass conservation 
  ! and assumes an isothermal transformation... 
  ! On exit, the gas velocity and sound speed are unchanged.
  ! New star particles are synchronized with other collisionless particles.
  ! Array flag2 is used as temporary work space.
  ! Yann Rasera  10/2002-01/2003
  !----------------------------------------------------------------------
  ! local constants
  real(dp) :: t0,d0,e0,mgas,mcell
  real(dp) :: scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v
  real(dp),dimension(1:twotondim,1:3) :: xc
  ! other variables
  integer  :: ncache,nnew,ivar,ngrid,icpu,index_star
  integer  :: igrid,ix,iy,iz,ind,i,j,n,iskip,istar,inew,nx_loc
  integer  :: ntot,ntot_all,info,nstar_corrected,ichem,ncell
#ifdef SOLVERhydro
  integer  :: imetal=6
#endif
#ifdef SOLVERmhd
  integer  :: imetal=9
#endif
  logical  :: ok_free,ok_all
  real(dp) :: d,x,y,z,u,v,w,e,zg,vdisp,dgas,feg,oxg
  real(dp) :: mstar,dstar,tstar,nISM,nCOM, scale_msun
  real(dp) :: velc,uc,vc,wc,mass_load,ul,vl,wl,ur,vr,wr,divv,curlv,alpha
  real(dp) :: vxgauss,vygauss,vzgauss,birth_epoch,factG
  real(kind=8) :: mlost,mtot,mlost_all,mtot_all,Temp
  real(kind=8) :: RandNum,GaussNum,PoissMean   
  real(dp),dimension(1:3)::skip_loc
  real(dp) :: dx,dx_loc,scale,vol_loc,dx_min,vol_min,d1,d2,d3,d4,d5,d6
  real(dp) :: bx1,bx2,by1,by2,bz1,bz2
  integer ,dimension(1:ncpu,1:IRandNumSize)    :: allseed
  integer ,dimension(1:nvector),save           :: ind_grid,ind_cell,nstar
  integer ,dimension(1:nvector),save           :: ind_grid_new,ind_cell_new,ind_part
  logical ,dimension(1:nvector),save           :: ok,ok_new=.true.,ok_true=.true.
  integer ,dimension(1:ncpu)                   :: ntot_star_cpu,ntot_star_all
  integer ,dimension(1:nvector,0:twondim)      :: ind_nbor
  real(dp) :: T2cell
  real(dp),dimension(1:nchem) :: chem1=0.0
  
  
  real(dp), dimension(3) :: r0hr
  real(dp) :: rad0hr, mtotHR, mtotHR_all, dmstar_tot, dmstar_all, dxhr, dyhr, dzhr
  
  if (numbtot(1,ilevel)==0) return
  if (.not. hydro) return
  if (ndim.ne.3) return


  if (verbose) write(*,*)' Entering star_formation'
    
  ! Conversion factor from user units to cgs units
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)
  scale_msun=(scale_d*scale_l**3d0)/2d33

  ! Mesh spacing in that level
  dx       = 0.5D0**ilevel 
  nx_loc   = (icoarse_max-icoarse_min+1)
  skip_loc = (/0.0d0,0.0d0,0.0d0/)
  if (ndim>0) skip_loc(1)=dble(icoarse_min)
  if (ndim>1) skip_loc(2)=dble(jcoarse_min)
  if (ndim>2) skip_loc(3)=dble(kcoarse_min)
  scale    = boxlen/dble(nx_loc)
  dx_loc   = dx*scale
  vol_loc  = dx_loc**ndim
  dx_min   = (0.5D0**nlevelmax)*scale
  vol_min  = dx_min**ndim

  r0hr(1) = 0.384844*scale
  r0hr(2) = 0.454605*scale
  r0hr(3) = 0.485074*scale
  rad0hr = 0.125*scale
  if (myid == 1) then
    write(*,*) "Using hardcoded HR region with r=0.5"
    write(*,*) "Scale = ", scale
  endif

  mtotHR = 0.d0
  mtotHR_all = 0.d0
  dmstar_tot = 0.d0
  dmstar_all = 0.d0

  ! Star formation time scale from Gyr to code units 
  t0    = t_star*(1d9*365.*24.*3600.)/scale_t
  ! ISM density threshold from H/cc to code units    
  nISM  = n_star
  nCOM  = del_star*omega_b*rhoc*(h0/100.)**2/aexp**3*XH/mH
  nISM  = MAX(nCOM,nISM)
  d0    = nISM/scale_nH
  mstar = n_star/(scale_nH*aexp**3)*vol_min
  ! Make sure m_star isn't too close to DM mass ... preferably <~1/10
  ! Also, m_star can't be more than mstar, because then you'll basically
  ! never have enough mass to actually make the particle
  if ( (m_star .gt. 0.d0) .and. (m_star .lt. mstar) ) then
    mstar = m_star/scale_msun
  endif
    
  dstar = mstar/vol_loc
  factG = 1d0
  if (cosmo) factG = 3d0/4d0/twopi*omega_m*aexp
  ! formation time of the star particle
  birth_epoch = t

  ! Cells center position relative to grid center position
  do ind=1,twotondim  
     iz        = (ind-1)/4
     iy        = (ind-1-4*iz)/2
     ix        = (ind-1-2*iy-4*iz)
     xc(ind,1) = (dble(ix)-0.5D0)*dx
     xc(ind,2) = (dble(iy)-0.5D0)*dx
     xc(ind,3) = (dble(iz)-0.5D0)*dx
  end do

  ! If necessary, initialize random number generator
  if (localseed(1)==-1) then
     call rans(ncpu,iseed,allseed)
     localseed = allseed(myid,1:IRandNumSize)
  end if

#if NDIM==3
  !------------------------------------------------
  ! Convert hydro variables to primitive variables
  !------------------------------------------------
  ncache = active(ilevel)%ngrid
  do igrid=1,ncache,nvector
     ngrid = MIN(nvector,ncache-igrid+1)
     do i=1,ngrid
        ind_grid(i) = active(ilevel)%igrid(igrid+i-1)
     end do
     do ind=1,twotondim  
        iskip = ncoarse+(ind-1)*ngridmax
        do i=1,ngrid
           ind_cell(i) = iskip+ind_grid(i)
        end do
        do i=1,ngrid
           d   = uold(ind_cell(i),1)
           u   = uold(ind_cell(i),2)/d
           v   = uold(ind_cell(i),3)/d
           w   = uold(ind_cell(i),4)/d
           e   = uold(ind_cell(i),5)/d
#ifdef SOLVERmhd
           bx1 = uold(ind_cell(i),6)
           by1 = uold(ind_cell(i),7)
           bz1 = uold(ind_cell(i),8)
           bx2 = uold(ind_cell(i),nvar+1)
           by2 = uold(ind_cell(i),nvar+2)
           bz2 = uold(ind_cell(i),nvar+3)
           e   = e-0.125d0*((bx1+bx2)**2+(by1+by2)**2+(bz1+bz2)**2)/d
#endif
           e   = e-0.5d0*(u**2+v**2+w**2)
           uold(ind_cell(i),1) = d
           uold(ind_cell(i),2) = u
           uold(ind_cell(i),3) = v
           uold(ind_cell(i),4) = w
           uold(ind_cell(i),5) = e
        end do
        do ivar=imetal,nvar
           do i=1,ngrid
              d = uold(ind_cell(i),1)
              w = uold(ind_cell(i),ivar)/d
              uold(ind_cell(i),ivar) = w
           end do
        end do
     end do
  end do

! get values of uold for density and velocities in virtual boundaries
#ifndef WITHOUTMPI
  do ivar=1,4
     call make_virtual_fine_dp(uold(1,ivar),ilevel)
  end do
#endif

  !------------------------------------------------
  ! Compute number of new stars in each cell
  !------------------------------------------------
  ntot = 0
  ! Loop over grids
  ncache = active(ilevel)%ngrid
  do igrid=1,ncache,nvector
     ngrid = MIN(nvector,ncache-igrid+1)
     do i=1,ngrid
        ind_grid(i) = active(ilevel)%igrid(igrid+i-1)
     end do
     ! Star formation criterion ---> logical array ok(i)
     do ind=1,twotondim
        iskip = ncoarse+(ind-1)*ngridmax
        do i=1,ngrid
           ind_cell(i) = iskip+ind_grid(i)
        end do
        ! Flag leaf cells
        do i=1,ngrid
           ok(i) = (son(ind_cell(i))==0)
        end do

        if(sf_hopkins)then
           ! Enforce self-gravitating criterion a la Hopkins et al 2013
           do i=1,ngrid
              ! if cell is a leaf cell
              if (ok(i)) then 
                 d = uold(ind_cell(i),1)
                 ! density and temp threshold to avoid computing the criterion too often but otherwise not needed
                 Temp = (gamma-1.0)*uold(ind_cell(i),5)*scale_T2
                 if (d <= d0 .or. (g_star == 1.0 .and. Temp > 1.1*T2_star)) then
                    ok(i) = .false.
                 else
                    ! we need velocity divergence and curl estimates in the cell, so construct values of velocity field 
                    ! on the 6 faces of the cell using simple linear interpolation from neighbouring cell values and differentiate.
                    ! get neighbor cells if they exist, otherwise use straight injection from local cell
                    ncell = 1 ! we just want the neighbors of that cell
                    call getnbor(ind_cell(i),ind_nbor,ncell,ilevel)
                    ! First calculate velocities for divergence
                    d1    = uold(ind_nbor(1,1),1)     ; d4    = uold(ind_nbor(1,4),1) 
                    d2    = uold(ind_nbor(1,2),1)     ; d5    = uold(ind_nbor(1,5),1) 
                    d3    = uold(ind_nbor(1,3),1)     ; d6    = uold(ind_nbor(1,6),1)  
                    ul    = (d2*uold(ind_nbor(1,2),2) + d*uold(ind_cell(i),2))/(d2+d)
                    vl    = (d4*uold(ind_nbor(1,4),3) + d*uold(ind_cell(i),3))/(d4+d)
                    wl    = (d6*uold(ind_nbor(1,6),4) + d*uold(ind_cell(i),4))/(d6+d)
                    ur    = (d1*uold(ind_nbor(1,1),2) + d*uold(ind_cell(i),2))/(d1+d)
                    vr    = (d3*uold(ind_nbor(1,3),3) + d*uold(ind_cell(i),3))/(d3+d)
                    wr    = (d5*uold(ind_nbor(1,5),4) + d*uold(ind_cell(i),4))/(d5+d)
                    divv  = ( ((ur-ul)+(vr-vl)+(wr-wl)) / dx_loc )**2 
                    ! Then calculate velocities for curl
                    vl    = (d6*uold(ind_nbor(1,6),3) + d*uold(ind_cell(i),3))/(d6+d)
                    wl    = (d4*uold(ind_nbor(1,4),4) + d*uold(ind_cell(i),4))/(d4+d)
                    vr    = (d5*uold(ind_nbor(1,5),3) + d*uold(ind_cell(i),3))/(d5+d)
                    wr    = (d3*uold(ind_nbor(1,3),4) + d*uold(ind_cell(i),4))/(d3+d)
                    curlv = ((wr-wl)-(vr-vl))**2         ! first term
                    ul    = (d6*uold(ind_nbor(1,6),2) + d*uold(ind_cell(i),2))/(d6+d)
                    wl    = (d2*uold(ind_nbor(1,2),4) + d*uold(ind_cell(i),4))/(d2+d)
                    ur    = (d5*uold(ind_nbor(1,5),2) + d*uold(ind_cell(i),2))/(d5+d)
                    wr    = (d1*uold(ind_nbor(1,1),4) + d*uold(ind_cell(i),4))/(d1+d)
                    curlv = curlv + ((ur-ul)-(wr-wl))**2 ! second term
                    ul    = (d4*uold(ind_nbor(1,4),2) + d*uold(ind_cell(i),2))/(d4+d)
                    vl    = (d2*uold(ind_nbor(1,2),3) + d*uold(ind_cell(i),3))/(d2+d)
                    ur    = (d3*uold(ind_nbor(1,3),2) + d*uold(ind_cell(i),2))/(d3+d)
                    vr    = (d1*uold(ind_nbor(1,1),3) + d*uold(ind_cell(i),3))/(d1+d)
                    curlv = curlv + ((vr-vl)-(ur-ul))**2 ! third term
                    curlv = curlv / dx_loc**2 
                    ! Check if gas in cell is self-gravitating (alpha < 1)
                    alpha = 0.5d0*(divv+curlv)/(factG*d)
                    if (alpha > 1d0) ok(i) = .false. 
                 endif
              endif
           end do
        else
           ! Density criterion
           do i=1,ngrid
              d=uold(ind_cell(i),1)
              ! compute temperature in K  
              Temp = (gamma-1.0)*uold(ind_cell(i),5)*scale_T2
              if (d<=d0 .or. (g_star == 1.0 .and. Temp > 1.1*T2_star)) ok(i)=.false.
           end do
        endif
     ! Calculate number of new stars in each cell using Poisson statistics
        do i=1,ngrid
           nstar(i) = 0
           if (ok(i)) then
              ! Compute mean number of events
              d         = uold(ind_cell(i),1)
              mcell     = d*vol_loc
              if(sf_hopkins)then
                 ! this is the free fall time of an homogeneous sphere                                                                                  
                 tstar  = 0.5427*sqrt(1.0/(factG*d))
              else
                 tstar  = t0*sqrt(d0/d)
              endif
              PoissMean = dtnew(ilevel)/tstar*mcell/mstar
              ! If catastrophic star formation (massive star cluster) wants to occur, we need to limit the                     
              ! maximal mass of the star particle we want to create in a cell for numerical reasons (gravity solver).                             
              PoissMean = min(PoissMean,max_nstar)
              ! Compute Poisson realisation
              call poissdev(localseed,PoissMean,nstar(i))
              ! Compute depleted gas mass
              mgas      = nstar(i)*mstar
              ! Security to prevent more than 90% of gas depletion
              if (mgas > 0.9*mcell) then
                 nstar_corrected = int(0.9*mcell/mstar)
                 mstar_lost      = mstar_lost+(nstar(i)-nstar_corrected)*mstar
                 nstar(i)        = nstar_corrected
              endif
              ! Compute new stars local statistics
              mstar_tot = mstar_tot+nstar(i)*mstar
              dmstar_tot = dmstar_tot+nstar(i)*mstar
              ! I want to do a counter for High Res region SF only
              x = (xg(ind_grid(i),1)+xc(ind,1)-skip_loc(1))*scale
              y = (xg(ind_grid(i),2)+xc(ind,2)-skip_loc(2))*scale
              z = (xg(ind_grid(i),3)+xc(ind,3)-skip_loc(3))*scale
 
              dxhr = x - r0hr(1)
              if (abs(dxhr) .gt. scale/2.d0) then
                dxhr = scale - abs(dxhr)
              endif

              dyhr = y - r0hr(2)
              if (abs(dyhr) .gt. scale/2.d0) then
                dyhr = scale - abs(dyhr)
              endif

              dzhr = z - r0hr(3)
              if (abs(dzhr) .gt. scale/2.d0) then
                dzhr = scale - abs(dzhr)
              endif

              if ( sqrt( dxhr**2 + dyhr**2 + dzhr**2 ) .le. &
                        rad0hr ) then
                mtotHR = mtotHR + nstar(i)*mstar
              endif
              
              if (nstar(i)>0) then
                 ntot = ntot+1
              endif
           endif
        enddo
        ! Store nstar in array flag2
        do i=1,ngrid
           flag2(ind_cell(i)) = nstar(i)
        end do
     end do
  end do

  !---------------------------------
  ! Check for free particle memory
  !---------------------------------
  ok_free = ((numbp_free-ntot)>=0)
#ifndef WITHOUTMPI
  call MPI_ALLREDUCE(numbp_free,numbp_free_tot,1,MPI_INTEGER,MPI_MIN,MPI_COMM_WORLD,info)
#endif
#ifdef WITHOUTMPI
  numbp_free_tot = numbp_free
#endif
  if(.not. ok_free)then
     write(*,*)'No more free memory for particles'
     write(*,*)'Increase npartmax'
#ifndef WITHOUTMPI
    call MPI_ABORT(MPI_COMM_WORLD,1,info)
#else
    stop
#endif
  end if

  !---------------------------------
  ! Compute global stars statistics
  !---------------------------------
#ifndef WITHOUTMPI
  mlost = mstar_lost; mtot = mstar_tot
  call MPI_ALLREDUCE(ntot,ntot_all,1,MPI_INTEGER,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(mtot,mtot_all,1,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(dmstar_tot,dmstar_all,1,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(mtotHR,mtotHR_all,1,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(mlost,mlost_all,1,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
#endif
#ifdef WITHOUTMPI
  ntot_all  = ntot
  mtot_all  = mstar_tot
  dmstar_all = dmstar_tot
  mtotHR_all = mtotHR
  mlost_all = mstar_lost
#endif
  ntot_star_cpu = 0; ntot_star_all = 0
  ntot_star_cpu(myid) = ntot
#ifndef WITHOUTMPI
  call MPI_ALLREDUCE(ntot_star_cpu,ntot_star_all,ncpu,MPI_INTEGER,MPI_SUM,MPI_COMM_WORLD,info)
  ntot_star_cpu(1) = ntot_star_all(1)
#endif
  do icpu=2,ncpu
     ntot_star_cpu(icpu) = ntot_star_cpu(icpu-1)+ntot_star_all(icpu)
  end do
  nstar_tot = nstar_tot+ntot_all
  if (myid==1) then
     if (ntot_all.gt.0) then
        write(*,'(" Level=",I6," New star=",I6," Tot=",I10," New mass=",1PE10.3, &
             & " Tot Mass=",1PE10.3," Lost=",0PF4.1,"%")')&
             & ilevel,ntot_all,nstar_tot,dmstar_all,mtot_all,mlost_all/mtot_all*100.
        write(*,'(" Level=",I6," New HR star Mass=",1PE10.3)')&
             & ilevel,mtotHR_all
     endif
  end if

  !------------------------------
  ! Create new star particles
  !------------------------------
  ! Starting identity number
  if (myid==1) then
     index_star = nstar_tot-ntot_all
  else
     index_star = nstar_tot-ntot_all+ntot_star_cpu(myid-1)
  end if

  ! Loop over grids
  ncache = active(ilevel)%ngrid
  do igrid=1,ncache,nvector
     ngrid = MIN(nvector,ncache-igrid+1)
     do i=1,ngrid
        ind_grid(i) = active(ilevel)%igrid(igrid+i-1)
     end do

     ! Loop over cells
     do ind=1,twotondim
        iskip = ncoarse+(ind-1)*ngridmax
        do i=1,ngrid
           ind_cell(i) = iskip+ind_grid(i)
        end do

        ! Flag cells with at least one new star
        do i=1,ngrid
           ok(i) = flag2(ind_cell(i))>0
        end do

        ! Gather new star arrays
        nnew = 0
        do i=1,ngrid
           if (ok(i)) then
              nnew               = nnew+1
              ind_grid_new(nnew) = ind_grid(i)
              ind_cell_new(nnew) = ind_cell(i)
           end if
        end do

        ! Update linked list for stars
        call remove_free(ind_part,nnew)
        call add_list(ind_part,ind_grid_new,ok_new,nnew)

        ! Calculate new star particle and modify gas density
        do i=1,nnew
           index_star = index_star+1

           ! Get gas variables
           n = flag2(ind_cell_new(i))
           d = uold(ind_cell_new(i),1)
           u = uold(ind_cell_new(i),2)
           v = uold(ind_cell_new(i),3)
           w = uold(ind_cell_new(i),4)
           x = (xg(ind_grid_new(i),1)+xc(ind,1)-skip_loc(1))*scale
           y = (xg(ind_grid_new(i),2)+xc(ind,2)-skip_loc(2))*scale
           z = (xg(ind_grid_new(i),3)+xc(ind,3)-skip_loc(3))*scale
           if (metal) then
              zg = uold(ind_cell_new(i),imetal)
              do ichem=1,nchem
                 chem1(ichem) = uold(ind_cell_new(i),imetal+ichem)
              enddo
           endif

           ! Set star particle variables
           tp(ind_part(i))     = birth_epoch  ! Birth epoch
           mp(ind_part(i))     = n*mstar      ! Mass
           levelp(ind_part(i)) = ilevel       ! Level
           idp(ind_part(i))    = -index_star  ! Star identity
           xp(ind_part(i),1)   = x
           xp(ind_part(i),2)   = y
           xp(ind_part(i),3)   = z
           vp(ind_part(i),1)   = u
           vp(ind_part(i),2)   = v
           vp(ind_part(i),3)   = w
           if (metal) then
              zp(ind_part(i)) = zg  ! Initial star metallicity
              do ichem=1,nchem
                 chp(ind_part(i),ichem) = chem1(ichem)  ! Initial chemical abudance
              enddo
           endif
        end do
        ! End loop over new star particles

        ! Modify gas density according to mass depletion
        do i=1,ngrid
           if (flag2(ind_cell(i))>0) then
              n = flag2(ind_cell(i))
              d = uold(ind_cell(i),1)
              uold(ind_cell(i),1) = d-n*dstar
           endif
        end do

     end do
     ! End loop over cells
  end do
  ! End loop over grids
  
  !---------------------------------------------------------
  ! Convert hydro variables back to conservative variables
  !---------------------------------------------------------
  ncache = active(ilevel)%ngrid
  do igrid=1,ncache,nvector
     ngrid = MIN(nvector,ncache-igrid+1)
     do i=1,ngrid
        ind_grid(i) = active(ilevel)%igrid(igrid+i-1)
     end do
     do ind=1,twotondim  
        iskip = ncoarse+(ind-1)*ngridmax
        do i=1,ngrid
           ind_cell(i) = iskip+ind_grid(i)
        end do
        do i=1,ngrid
           d   = uold(ind_cell(i),1)
           u   = uold(ind_cell(i),2)
           v   = uold(ind_cell(i),3)
           w   = uold(ind_cell(i),4)
           e   = uold(ind_cell(i),5)
#ifdef SOLVERmhd
           bx1 = uold(ind_cell(i),6)
           by1 = uold(ind_cell(i),7)
           bz1 = uold(ind_cell(i),8)
           bx2 = uold(ind_cell(i),nvar+1)
           by2 = uold(ind_cell(i),nvar+2)
           bz2 = uold(ind_cell(i),nvar+3)
           e   = e+0.125d0*((bx1+bx2)**2+(by1+by2)**2+(bz1+bz2)**2)/d
#endif
           e   = e+0.5d0*(u**2+v**2+w**2)
           uold(ind_cell(i),1) = d
           uold(ind_cell(i),2) = d*u
           uold(ind_cell(i),3) = d*v
           uold(ind_cell(i),4) = d*w
           uold(ind_cell(i),5) = d*e
        end do
        do ivar=imetal,nvar
           do i=1,ngrid
              d = uold(ind_cell(i),1)
              w = uold(ind_cell(i),ivar)
              uold(ind_cell(i),ivar) = d*w
           end do
        end do
     end do
  end do

#endif

end subroutine star_formation 
!################################################################
!################################################################
!################################################################
!################################################################
subroutine getnbor(ind_cell,ind_father,ncell,ilevel)
  use amr_commons
  implicit none
  integer::ncell,ilevel
  integer,dimension(1:nvector)::ind_cell
  integer,dimension(1:nvector,0:twondim)::ind_father
  !-----------------------------------------------------------------
  ! This subroutine determines the 2*ndim neighboring cells
  ! cells of the input cell (ind_cell). 
  ! If for some reasons they don't exist, the routine returns 
  ! the input cell.
  !-----------------------------------------------------------------
  integer::nxny,i,idim,j,iok,ind
  integer,dimension(1:3)::ibound,iskip1,iskip2
  integer,dimension(1:nvector,1:3),save::ix
  integer,dimension(1:nvector),save::ind_grid_father,pos
  integer,dimension(1:nvector,0:twondim),save::igridn,igridn_ok
  integer,dimension(1:nvector,1:twondim),save::icelln_ok


  if(ilevel==1)then 
     write(*,*) 'Warning: attempting to form stars on level 1 --> this is not allowed ...'
     return
  endif

  ! Get father cell
  do i=1,ncell
     ind_father(i,0)=ind_cell(i)
  end do
  
  ! Get father cell position in the grid
  do i=1,ncell
     pos(i)=(ind_father(i,0)-ncoarse-1)/ngridmax+1
  end do
  
  ! Get father grid
  do i=1,ncell
     ind_grid_father(i)=ind_father(i,0)-ncoarse-(pos(i)-1)*ngridmax
  end do
  
  ! Get neighboring father grids
  call getnborgrids(ind_grid_father,igridn,ncell)
  
  ! Loop over position
  do ind=1,twotondim
     
     ! Select father cells that sit at position ind
     do j=0,twondim
        iok=0
        do i=1,ncell
           if(pos(i)==ind)then
              iok=iok+1
              igridn_ok(iok,j)=igridn(i,j)
           end if
        end do
     end do
     
     ! Get neighboring cells for selected cells
     if(iok>0)call getnborcells(igridn_ok,ind,icelln_ok,iok)
     
     ! Update neighboring father cells for selected cells
     do j=1,twondim
        iok=0
        do i=1,ncell
           if(pos(i)==ind)then
              iok=iok+1
              if(icelln_ok(iok,j)>0)then
                 ind_father(i,j)=icelln_ok(iok,j)
                 !write(*,*) 'index first if',ind_father(i,j) 
              else
                 ind_father(i,j)=ind_cell(i)
                 !write(*,*) 'index second if',ind_father(i,j) 
              end if
           end if
        end do
     end do
     
  end do
     
    
end subroutine getnbor
!##############################################################
