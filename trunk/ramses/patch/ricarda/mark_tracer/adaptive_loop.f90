subroutine adaptive_loop
  use amr_commons
  use hydro_commons
  use pm_commons
  use poisson_commons
  use cooling_module
  use stellar_commons
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
#endif
  integer::ilevel,idim,ivar,info, i_n, i_T
  real(kind=8)::tt1,tt2
  real(kind=4)::real_mem,real_mem_tot
  real(kind=8)::ttend
  real(kind=8),save::ttstart=0
  real_mem=0
  real_mem_tot=0
#ifndef WITHOUTMPI
  tt1=MPI_WTIME(info)
#endif

  call init_amr                      ! Initialize AMR variables
  call init_time                     ! Initialize time variables
  if(hydro)call init_hydro           ! Initialize hydro variables
  if(poisson)call init_poisson       ! Initialize poisson variables
  if(nrestart==0)call init_refine    ! Build initial AMR grid
  if(cooling) then
    call set_table(dble(aexp))  ! Initialize cooling look up table
#ifdef DEBUG_COOL2
    if (myid .eq. 1) then
        write(6,*) "Opening cooling table files at aexp=", aexp
        open(4042,file="tabDens.txt",status='unknown',access='APPEND')
        open(4142,file="tabTemp2.txt",status='unknown',access='APPEND')
        open(4242,file="tabMu.txt",status='unknown',access='APPEND')
        open(4342,file="tabCool.txt",status='unknown',access='APPEND')
        open(4442,file="tabHeat.txt",status='unknown',access='APPEND')
        open(4542,file="tabMetl.txt",status='unknown',access='APPEND')
        open(4642,file="tabCool_p.txt",status='unknown',access='APPEND')
        open(4742,file="tabHeat_p.txt",status='unknown',access='APPEND')
        open(4842,file="tabMetl_p.txt",status='unknown',access='APPEND')
     
        do i_T=1,table%n2
          write(4142,*) i_T, table%T2(i_T)
     
          do i_n=1,table%n1
            write(4242,*) i_n, i_T, table%mu(i_n,i_T)
            write(4342,*) i_n, i_T, table%cool(i_n,i_T)
            write(4442,*) i_n, i_T, table%heat(i_n,i_T)
            write(4542,*) i_n, i_T, table%metal(i_n,i_T)
            write(4642,*) i_n, i_T, table%cool_prime(i_n,i_T)
            write(4742,*) i_n, i_T, table%heat_prime(i_n,i_T)
            write(4842,*) i_n, i_T, table%metal_prime(i_n,i_T)
          enddo
        enddo

        do i_n=1,table%n1
          write(4042,*) i_n, table%nH(i_n)
        enddo

        close(4042)
        close(4142)
        close(4242)
        close(4342)
        close(4442)  
        close(4542)
        close(4642)
        close(4742)
        close(4842)
        stop
    endif
#endif
  endif
  if(pic)call init_part              ! Initialize particle variables
  if(pic)call init_tree              ! Initialize particle tree
  if(nrestart==0)call init_refine_2  ! Build initial AMR grid again
  if(star)call init_stellar_library  ! Initialize stellar library

#ifndef WITHOUTMPI
  tt2=MPI_WTIME(info)
  if(myid==1)write(*,*)'Time elapsed since startup:',tt2-tt1
#endif

  if(myid==1)then
     write(*,*)'Initial mesh structure'
     do ilevel=1,nlevelmax
        if(numbtot(1,ilevel)>0)write(*,999)ilevel,numbtot(1:4,ilevel)
     end do
  end if

  nstep_coarse_old=nstep_coarse

  if(myid==1)write(*,*)'Starting time integration' 

  do ! Main time loop

#ifndef WITHOUTMPI
     tt1=MPI_WTIME(info)
#endif

     if(verbose)write(*,*)'Entering amr_step_coarse'

     epot_tot=0.0D0  ! Reset total potential energy
     ekin_tot=0.0D0  ! Reset total kinetic energy
     mass_tot=0.0D0  ! Reset total mass
     eint_tot=0.0D0  ! Reset total internal energy
#ifdef SOLVERmhd
     emag_tot=0.0D0  ! Reset total magnetic energy
#endif

     ! Make new refinements
     if(levelmin.lt.nlevelmax)then
        call refine_coarse
        do ilevel=1,levelmin
           call build_comm(ilevel)
           call make_virtual_fine_int(cpu_map(1),ilevel)
           if(hydro)then
#ifdef SOLVERmhd
              do ivar=1,nvar+3
#else
              do ivar=1,nvar
#endif
                 call make_virtual_fine_dp(uold(1,ivar),ilevel)
              end do
              if(simple_boundary)call make_boundary_hydro(ilevel)
              if(poisson)then
                 do idim=1,ndim
                    call make_virtual_fine_dp(f(1,idim),ilevel)
                 end do
              end if
           end if
           if(ilevel<levelmin)call refine_fine(ilevel)
        end do
     endif

     ! Call base level
     call amr_step(levelmin,1)

     if(levelmin.lt.nlevelmax)then
        ! Hydro book-keeping
        if(hydro)then
           do ilevel=levelmin-1,1,-1
              call upload_fine(ilevel)
#ifdef SOLVERmhd
              do ivar=1,nvar+3
#else
              do ivar=1,nvar
#endif
                 call make_virtual_fine_dp(uold(1,ivar),ilevel)
              end do
              if(simple_boundary)call make_boundary_hydro(ilevel)
              if(poisson)then
                 do idim=1,ndim
                    call make_virtual_fine_dp(f(1,idim),ilevel)
                 end do
              end if
           end do
        end if
        
        ! Build refinement map
        do ilevel=levelmin-1,1,-1
           call flag_fine(ilevel,2)
        end do
        call flag_coarse
     endif

     ! New coarse time-step
     nstep_coarse=nstep_coarse+1

#ifndef WITHOUTMPI
     tt2=MPI_WTIME(info)
     if(mod(nstep_coarse,ncontrol)==0)then
        !call getmem(real_mem)
        call MPI_ALLREDUCE(real_mem,real_mem_tot,1,MPI_REAL,MPI_MAX,MPI_COMM_WORLD,info)
        if(myid==1)then
           write(*,*)'Time elapsed since last coarse step:',tt2-tt1
           call writemem(real_mem_tot)
        endif
     endif
#endif

#ifdef PlotCellParticles
  if (nstep_coarse .gt. 100) then
    call plot_cell_particles()

    !---------------
    ! Exit program
    !---------------
    if(myid==1)then
      write(*,*)'Run completed'
#ifndef WITHOUTMPI
      ttend=MPI_WTIME(info)
      write(*,*)'Total elapsed time:',ttend-ttstart
#endif
    endif
    call clean_stop
  endif
#endif

  end do

999 format(' Level ',I2,' has ',I10,' grids (',3(I8,','),')')

end subroutine adaptive_loop
