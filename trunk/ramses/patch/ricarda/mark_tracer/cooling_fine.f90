subroutine cooling_fine(ilevel)
  use amr_commons
  use hydro_commons
  use cooling_module
  use pm_commons, ONLY:xp,tp,idp
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
#endif
  integer::ilevel
  !-------------------------------------------------------------------
  ! Compute cooling for fine levels
  !-------------------------------------------------------------------
  integer::ncache,i,igrid,ngrid,info,isink
  integer,dimension(1:nvector),save::ind_grid
  real(dp),allocatable::xx_yso(:,:),tp_yso(:) ! added by TS
  integer::nstar_yso,j
  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v,t_off,t_yso
  real(kind=8)::dtemptot, dtempme
  integer::ntemptot,ntempme
#ifdef DEBUG_COOL3
  real(kind=8):: coolMe,heatMe,metlMe,cooltot,heattot,metltot, &
              coolMe_p,heatMe_p,metlMe_p,cooltot_p,heattot_p,metltot_p

  coolMe = 0.d0
  cooltot = 0.d0
  coolMe_p = 0.d0
  cooltot_p = 0.d0
  heatMe = 0.d0
  heattot = 0.d0
  heatMe_p = 0.d0
  heattot_p = 0.d0
  metlMe = 0.d0
  metltot = 0.d0
  metlMe_p = 0.d0
  metltot_p = 0.d0
#endif
  dtemptot = 0.d0
  dtempme  = 0.d0
  ntemptot = 0
  ntempme  = 0

  if(numbtot(1,ilevel)==0)return
  if(verbose)write(*,111)ilevel

  ! Find stars younger than 50 Myr  (added by TS)
  ! Note this is only valid for non-cosmological simulations
      ! Conversion factor from user units to cgs units
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)

  t_yso=50D0*1D6*(365D0*24D0*3600D0)/scale_t
  t_off=max(t-t_yso,1D-15)
  nstar_yso=count(tp.ge.t_off.and.idp.ne.0)
  if (nstar_yso>0) then 
     allocate(xx_yso(1:nstar_yso,1:ndim))
     allocate(tp_yso(1:nstar_yso))
     xx_yso=0;tp_yso=0
     i=1;j=1
     do while (j<=nstar_yso)
       if (tp(i).ge.t_off.and.idp(i).ne.0) then
         xx_yso(j,1)=xp(i,1)
         xx_yso(j,2)=xp(i,2)
         xx_yso(j,3)=xp(i,3)
         tp_yso(j  )=tp(i)
         j=j+1
       endif
       i=i+1
     end do
  else
    allocate(xx_yso(1,ndim))
    allocate(tp_yso(1))
    nstar_yso=-1
  endif

  ! Operator splitting step for cooling source term
  ! by vector sweeps
  ncache=active(ilevel)%ngrid
  do igrid=1,ncache,nvector
     ngrid=MIN(nvector,ncache-igrid+1)
     do i=1,ngrid
        ind_grid(i)=active(ilevel)%igrid(igrid+i-1)
     end do
#ifndef DEBUG_COOL3
     call coolfine1(ind_grid,ngrid,ilevel,xx_yso,tp_yso,nstar_yso,dtempme,ntempme)
#else
     call coolfine1(ind_grid,ngrid,ilevel,xx_yso,tp_yso,nstar_yso,dtempme,ntempme, &
                    coolMe,heatMe,metlMe,coolMe_p,heatMe_p,metlMe_p)
#endif
  end do

#ifndef WITHOUTMPI
  call MPI_ALLREDUCE(dtempme, dtemptot, 1, MPI_DOUBLE_PRECISION, &
                       MPI_SUM, MPI_COMM_WORLD, info)
#ifdef DEBUG_COOL3
  call MPI_ALLREDUCE(coolMe, cooltot, 1, MPI_DOUBLE_PRECISION, &
                       MPI_SUM, MPI_COMM_WORLD, info)
  call MPI_ALLREDUCE(heatMe, heattot, 1, MPI_DOUBLE_PRECISION, &
                       MPI_SUM, MPI_COMM_WORLD, info)
  call MPI_ALLREDUCE(metlMe, metltot, 1, MPI_DOUBLE_PRECISION, &
                       MPI_SUM, MPI_COMM_WORLD, info)
  call MPI_ALLREDUCE(coolMe_p, cooltot_p, 1, MPI_DOUBLE_PRECISION, &
                       MPI_SUM, MPI_COMM_WORLD, info)
  call MPI_ALLREDUCE(heatMe_p, heattot_p, 1, MPI_DOUBLE_PRECISION, &
                       MPI_SUM, MPI_COMM_WORLD, info)
  call MPI_ALLREDUCE(metlMe_p, metltot_p, 1, MPI_DOUBLE_PRECISION, &
                       MPI_SUM, MPI_COMM_WORLD, info)
#endif
  call MPI_ALLREDUCE(ntempme, ntemptot, 1, MPI_INTEGER, &
                       MPI_SUM, MPI_COMM_WORLD, info)
#else
  dtemptot = dtempme
#ifdef DEBUG_COOL3
  cooltot = coolMe
  heattot = heatMe
  metltot = metlMe
  cooltot_p = coolMe_p
  heattot_p = heatMe_p
  metltot_p = metlMe_p
#endif
  ntemptot = ntempme
#endif
  if (myid .eq. 1) then
    write(6,*) "COOLFINE In level ", ilevel, " dtemp is ", dtemptot
#ifdef DEBUG_COOL3
    write(6,*) "COOLFINE cool, cool_p = ", cooltot, cooltot_p
    write(6,*) "COOLFINE heat, heat_p = ", heattot, heattot_p
    write(6,*) "COOLFINE metl, metl_p = ", metltot, metltot_p
#endif
    write(6,*) "COOLFINE In level ", ilevel, " ntemp is ", ntemptot
    write(6,*) "COOLFINE: ierr = ", info 
  endif

  if(cooling.and.ilevel==levelmin.and.cosmo)then
     if(myid==1)write(*,*)'Computing new cooling table'
     call set_table(dble(aexp))
  end if

  deallocate(xx_yso,tp_yso)

111 format('   Entering cooling_fine for level',i2)

end subroutine cooling_fine
!###########################################################
!###########################################################
!###########################################################
!###########################################################
#ifndef DEBUG_COOL3
subroutine coolfine1(ind_grid,ngrid,ilevel,xx_yso,tp_yso,nstar_yso,dtempme,ntempme)
#else
subroutine coolfine1(ind_grid,ngrid,ilevel,xx_yso,tp_yso,nstar_yso,dtempme,ntempme, &
                     coolMe,heatMe,metlMe,coolMe_p,heatMe_p,metlMe_p)
#endif
  use amr_commons
  use hydro_commons
  use cooling_module
  implicit none

  integer::ilevel,ngrid
  integer,dimension(1:nvector)::ind_grid
  !-------------------------------------------------------------------
  !-------------------------------------------------------------------
  integer::i,ind,iskip,idim,nleaf,nx_loc,ix,iy,iz
  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v
  real(kind=8)::dtcool,nISM,nCOM,t_yso,t_off
  real(dp),dimension(1:nvector,1:ndim),save::xx
  integer,dimension(1:nvector),save::ind_cell,ind_leaf
  real(kind=8),dimension(1:nvector),save::nH,T2,delta_T2,ekk,T2min,Zsolar
  real(dp),dimension(1:twotondim,1:3)::xc
  real(dp),dimension(1:3)::skip_loc
  real(kind=8)::dx,dx_loc,scale,vol_loc
  integer::nstar_yso,iStromgren(1:nvector)  ! added by TS
  real(dp)::xx_yso(1:abs(nstar_yso),1:ndim),tp_yso(1:abs(nstar_yso))
  real(dp)::r_stromgren,ethmax_fix
  real(kind=8)::dtempme
  integer ::ntempme
#ifdef DEBUG_COOL3
  real(kind=8)::coolMe,heatMe,metlMe,coolMe_p,heatMe_p,metlMe_p
  integer :: iNH_1, iT2_1
  real    :: dlog_NH, dlog_T2

  dlog_NH = (table%n1-1)/(table%nH(table%n1)-table%nH(1))
  dlog_T2 = (table%n2-1)/(table%T2(table%n2)-table%T2(1))  
#endif

  ! Mesh spacing in that level
  dx=0.5D0**ilevel 
  nx_loc=(icoarse_max-icoarse_min+1)
  skip_loc=(/0.0d0,0.0d0,0.0d0/)
  if(ndim>0)skip_loc(1)=dble(icoarse_min)
  if(ndim>1)skip_loc(2)=dble(jcoarse_min)
  if(ndim>2)skip_loc(3)=dble(kcoarse_min)
  scale=boxlen/dble(nx_loc)
  dx_loc=dx*scale
  vol_loc=dx_loc**ndim

  ! Set position of cell centers relative to grid center
  do ind=1,twotondim
     iz=(ind-1)/4
     iy=(ind-1-4*iz)/2
     ix=(ind-1-2*iy-4*iz)
     if(ndim>0)xc(ind,1)=(dble(ix)-0.5D0)*dx
     if(ndim>1)xc(ind,2)=(dble(iy)-0.5D0)*dx
     if(ndim>2)xc(ind,3)=(dble(iz)-0.5D0)*dx
  end do

  ! Conversion factor from user units to cgs units
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)

  r_stromgren = 100.*3.08D18/scale_l

 
  ! Typical ISM density in H/cc
  nISM = n_star; nCOM=0d0
  if(cosmo)then
     nCOM = del_star*omega_b*rhoc*(h0/100.)**2/aexp**3*X/mH
  endif
  nISM = MAX(nCOM,nISM)

  ! Loop over cells
  do ind=1,twotondim
     iskip=ncoarse+(ind-1)*ngridmax
     do i=1,ngrid
        ind_cell(i)=iskip+ind_grid(i)
     end do

     ! Gather leaf cells
     nleaf=0
     do i=1,ngrid
        if(son(ind_cell(i))==0)then
           nleaf=nleaf+1
           ind_leaf(nleaf)=ind_cell(i)
           do idim=1,ndim
              xx(nleaf,idim)=(xg(ind_grid(i),idim)+xc(ind,idim)-skip_loc(idim))*scale
           end do
        end if
     end do

     ! find the cells that are within strongen cube (not sphere..) (added by TS)

     ! Compute rho
     do i=1,nleaf
        nH(i)=MAX(uold(ind_leaf(i),1),smallr)
     end do
     
     ! Compute metallicity in solar units
     if(metal)then
        do i=1,nleaf
           Zsolar(i)=uold(ind_leaf(i),ndim+3)/nH(i)/0.02
        end do
     else
        do i=1,nleaf
           Zsolar(i)=z_ave
        end do
     endif

     ! Compute pressure
     do i=1,nleaf
        T2(i)=uold(ind_leaf(i),ndim+2)
     end do
     do i=1,nleaf
        ekk(i)=0.0d0
     end do
     do idim=1,ndim
        do i=1,nleaf
           ekk(i)=ekk(i)+0.5*uold(ind_leaf(i),idim+1)**2/nH(i)
        end do
     end do
     do i=1,nleaf
        T2(i)=(gamma-1.0)*(T2(i)-ekk(i))
     end do

     ! Compute T2=T/mu in Kelvin
     do i=1,nleaf
        T2(i)=T2(i)/nH(i)*scale_T2
     end do

     ! Compute nH in H/cc
     do i=1,nleaf
        nH(i)=nH(i)*scale_nH
     end do

#ifdef DEBUG_COOL3
     do i=1,nleaf
       iNH_1 = MIN(MAX(int((log10(nH(i))-table%nH(1))*dlog_nH)+1,1),table%n1-1)
       iT2_1 = MIN(MAX(int((log10(T2(i))-table%T2(1))*dlog_T2)+1,1),table%n2-1)

       write(4242,*) iNH_1, iT2_1, log10(nH(i)), log10(T2(i))
     enddo
#endif

     !==========================================
     ! Compute temperature from polytrope EOS
     !==========================================
     T2min=0d0
     do i=1,nleaf
        T2min(i) = T2_star*(nH(i)/nISM)**(g_star-1.0)
        if (cooling) T2min(i)=T2min(i)+T2_min_fix
     end do


     !==========================================
     ! You can put your own polytrope EOS here
     !==========================================
     ! Compute cooling time step in second
     dtcool = dtnew(ilevel)*scale_t

     ! Compute net cooling at constant nH
     if(cooling)then
        do i=1,nleaf
           T2(i)=MAX(T2(i),T2min(i))
        end do
#ifndef DEBUG_COOL3
        call solve_cooling(nH,T2,Zsolar,dtcool,delta_T2,nleaf)
#else
        call solve_cooling(nH,T2,Zsolar,dtcool,delta_T2,nleaf,coolMe,heatMe,metlMe, &
                           coolMe_p, heatMe_p, metlMe_p)
#endif
        do i=1,nleaf
          dtempme = dtempme - delta_T2(i)
          ntempme = ntempme + 1
        enddo 
    endif

     ! Compute rho
     do i=1,nleaf
        nH(i) = nH(i)/scale_nH
     end do

     ! Compute net energy sink
     if(cooling)then
        do i=1,nleaf
           delta_T2(i) = delta_T2(i)*nH(i)/scale_T2/(gamma-1.0)
        end do
     endif

     if(cooling.and.delayed_cooling)then
!       t_off=50.*1d6*(365.*24.*3600.)/scale_t
!       do i=1,nleaf
!          t_yso=(1d0/(uold(ind_leaf(i),ndim+4) &
!               & /max(uold(ind_leaf(i),1),smallr)+1d-15)+t)
!          if(t_yso<t_off)delta_T2(i)=0
!       end do
        iStromgren=-1
        do i=1,nstar_yso
           where( abs(xx(:,1)-xx_yso(i,1)).le.r_stromgren.and.&
                & abs(xx(:,2)-xx_yso(i,2)).le.r_stromgren.and.&
                & abs(xx(:,3)-xx_yso(i,3)).le.r_stromgren) iStromgren=1
        enddo

        do i=1,nleaf
          if (iStromgren(i)>0.and.T2(i).le.1D4) delta_T2(i)=0
        enddo
     endif

     ! Update total fluid energy
     do i=1,nleaf
        T2(i) = uold(ind_leaf(i),ndim+2)
     end do

     if(cooling)then
        do i=1,nleaf
           T2(i) = T2(i)+delta_T2(i)
        end do
     endif

     ! update hydro variables
     if(isothermal)then
        do i=1,nleaf
           uold(ind_leaf(i),ndim+2) = T2min(i)
        end do
     else
        ! Compute minimal total energy from polytrope
        do i=1,nleaf
           T2min(i) = T2min(i)*nH(i)/scale_T2/(gamma-1.0) + ekk(i)
        end do
        do i=1,nleaf
           uold(ind_leaf(i),ndim+2) = max(T2(i),T2min(i))
        end do
     endif

     if(T2max_fix.gt.0)then
        do i=1,nleaf
           ethmax_fix=T2max_fix*nH(i)/scale_T2/(gamma-1.0) + ekk(i)
           uold(ind_leaf(i),ndim+2) = min(uold(ind_leaf(i),ndim+2),ethmax_fix)
        end do
     endif
  end do
  ! End loop over cells

end subroutine coolfine1
