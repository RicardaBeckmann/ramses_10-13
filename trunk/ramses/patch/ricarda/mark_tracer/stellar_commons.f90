module stellar_commons
   use amr_parameters
   integer, parameter::nbint=100
   integer, parameter::nbinz=5
   real(KIND=8),dimension(1:nbint)        :: t_wind
   real(KIND=8),dimension(1:nbinz)        :: Z_wind
   real(KIND=8),dimension(1:nbint,1:nbinz):: cMwind    !cumulative mass
   real(KIND=8),dimension(1:nbint,1:nbinz):: cEwind    !cumulative energy
   real(KIND=8),dimension(1:nbint,1:nbinz):: cMwind_Z  !cumulative mass*metal
   real(KIND=8),dimension(:,:,:),allocatable:: cMwind_chem
   real(KIND=8),dimension(1:nchem+1)::mZ_Ia
end module stellar_commons
