!=======================================================================
subroutine title8(n,nchar)
!=======================================================================
  implicit none
  integer::n
  character(LEN=8)::nchar

  character(LEN=1)::nchar1
  character(LEN=2)::nchar2
  character(LEN=3)::nchar3
  character(LEN=4)::nchar4
  character(LEN=5)::nchar5
  character(LEN=6)::nchar6
  character(LEN=7)::nchar7
  character(LEN=8)::nchar8

  if (n.ge.100000000) then
     write(6,*) "WARNING: Sink ID > 10^8: ", n
     write(6,*) "WARNING:  dividing into (10^7 : 10^8) range"
     write(nchar8,'(i8)') n/10**( int(log10(dble(n)))-7 )
     nchar = nchar8
  elseif(n.ge.10000000)then
     write(nchar8,'(i8)') n
     nchar = nchar8
  elseif(n.ge.1000000)then
     write(nchar7,'(i7)') n
     nchar = '0'//nchar7
  elseif(n.ge.100000)then
     write(nchar6,'(i6)') n
     nchar = '00'//nchar6
  elseif(n.ge.10000)then
     write(nchar5,'(i5)') n
     nchar = '000'//nchar5
  elseif(n.ge.1000)then
     write(nchar4,'(i4)') n
     nchar = '0000'//nchar4
  elseif(n.ge.100)then
     write(nchar3,'(i3)') n
     nchar = '00000'//nchar3
  elseif(n.ge.10)then
     write(nchar2,'(i2)') n
     nchar = '000000'//nchar2
  elseif (n.ge.0)then
     write(nchar1,'(i1)') n
     nchar = '0000000'//nchar1
  else
     write(6,*) "WARNING: Sink id n < 0: ", n
     write(6,*) "WARNING: Taking negative id"
     n = -n
     if (n.ge.100000000) then
        write(6,*) "WARNING: Sink ID > 10^8: ", n
        write(6,*) "WARNING:  dividing into (10^7 : 10^8) range"
        write(nchar8,'(i8)') n/10**( int(log10(dble(n)))-7 )
        nchar = nchar8
     elseif(-n.ge.10000000)then
        write(nchar8,'(i8)') n
        nchar = nchar8
     elseif(n.ge.1000000)then
        write(nchar7,'(i7)') n
        nchar = '0'//nchar7
     elseif(n.ge.100000)then
        write(nchar6,'(i6)') n
        nchar = '00'//nchar6
     elseif(n.ge.10000)then
        write(nchar5,'(i5)') n
        nchar = '000'//nchar5
     elseif(n.ge.1000)then
        write(nchar4,'(i4)') n
        nchar = '0000'//nchar4
     elseif(n.ge.100)then
        write(nchar3,'(i3)') n
        nchar = '00000'//nchar3
     elseif(n.ge.10)then
        write(nchar2,'(i2)') n
        nchar = '000000'//nchar2
     else
        write(nchar1,'(i1)') n
        nchar = '0000000'//nchar1
     endif
     n = -n
  endif


end subroutine title8

