subroutine plot_cell_particles()
  use pm_commons
  use amr_commons
  use cooling_module, ONLY: XH=>X, rhoc, mH , twopi
  implicit none

  !------------------------------------------------------------------------
  ! This routine computes the parameters of Bondi-Hoyle
  ! accretion for sink particles.
  ! It calls routine bondi_veocity and average_density.
  !------------------------------------------------------------------------
  integer::ilevel
  integer::igrid,jgrid,ipart,jpart,next_part,idim,info
  integer::i,ig,ip,npart1,npart2,icpu,nx_loc,isink
  integer,dimension(1:nvector),save::ind_grid,ind_part,ind_grid_part
  real(dp)::r2,dx_loc,dx_min,scale
  real(dp)::dx,factG,pi
  real(dp)::RandNum,phi,Rrand,SS,CC,UU,csound,turb,ttsta,ttend
  integer ,dimension(1:ncpu,1:IRandNumSize)::allseed
  logical :: notdone =.true.
  if (myid .ne. 1) return

  open(42,file="Cell_Particles.txt",status="unknown")

  ! Find the maximum level with particles.
  do ilevel=nlevelmax,levelmin,-1
    if (numbtot(1,ilevel) .ne. 0) exit
  enddo
  ilevel = max(ilevel,levelmin)
  do while (notdone)

  write(6,*) "Using an ilevel of ", ilevel
 
  ! Mesh spacing in that level
  dx=0.5D0**ilevel
  nx_loc=(icoarse_max-icoarse_min+1)
  scale=boxlen/dble(nx_loc)
  dx_min=scale*0.5d0**nlevelmax/aexp

  ! Let's settle on a grid and gather its particles:
  ! Loop over cpus
  do icpu=1,ncpu
     igrid=headl(icpu,ilevel)
     ig=0
     ip=0
     ! Loop over grids
     write(6,*) "icpu igrid ngrid = ", icpu, igrid, numbp(igrid)
     do jgrid=1,numbl(icpu,ilevel)
        npart1=numbp(igrid)  ! Number of particles in the grid
        npart2=0

        ! Count sink and cloud particles
        if(npart1>0)then
           ipart=headp(igrid)
           ! Loop over particles
           do jpart=1,npart1
              ! Save next particle   <--- Very important !!!
              next_part=nextp(ipart)
              if(idp(ipart).gt.0 .and. tp(ipart).eq.0.d0)then
                 npart2=npart2+1
              endif
              ipart=next_part  ! Go to next particle
           end do
        endif

        ! Gather dm particles
        if(npart2>0)then
           notdone = .false.
           write(42,'(a)') "# ilevel icpu igrid ngrid npart1 npart2"   
           write(42,'(a,6i)') "# ", ilevel, icpu, igrid, numbl(icpu,ilevel),npart1, npart2   
           write(42,'(a)') "# xg yg zg dx"
           write(42,'(a,4es12.4)') "# ",xg(igrid,1:ndim),dx
           close(42)
           ig=ig+1
           ind_grid(ig)=igrid
           ipart=headp(igrid)

           ! Loop over particles
           do jpart=1,npart1
              ! Save next particle   <--- Very important !!!
              next_part=nextp(ipart)
              ! Select only sink particles
              if(idp(ipart).gt.0 .and. tp(ipart).eq.0.d0)then
                 if(ig==0)then
                    ig=1
                    ind_grid(ig)=igrid
                 end if
                 ip=ip+1
                 ind_part(ip)=ipart
                 ind_grid_part(ip)=ig
              endif
              if(ip==nvector)then
                 call write_chunk(ind_grid,ind_part,ind_grid_part,ig,ip,ilevel)
                 ip=0
                 ig=0
              end if
              ipart=next_part  ! Go to next particle
           end do
           ! End loop over particle
           ! I only want to do one grid!
      
           ! End loop over grids
           if(ip>0) call write_chunk(ind_grid,ind_part,ind_grid_part,ig,ip,ilevel)
           exit

        end if

        igrid=next(igrid)   ! Go to next grid
     end do
     if (jgrid .le. numbl(icpu,ilevel) ) exit

  enddo

  if (notdone) then
    ilevel = ilevel - 1
    if (ilevel .lt. levelmin) notdone = .false.
  endif

  enddo

end subroutine plot_cell_particles

subroutine write_chunk(ind_grid,ind_part,ind_grid_part,ng,np,ilevel)
  use amr_commons
  use pm_commons
  use hydro_commons
  use cooling_module, ONLY: XH=>X, rhoc, mH
  implicit none
  integer::ng,np,ilevel
  integer,dimension(1:nvector)::ind_grid
  integer,dimension(1:nvector)::ind_grid_part,ind_part
  !-----------------------------------------------------------------------
  ! This routine is called by subroutine bondi_hoyle. Each cloud particle
  ! reads up the value of density, sound speed and velocity from its
  ! position in the grid.
  !-----------------------------------------------------------------------
  integer::i,j,idim,nx_loc
  real(dp)::dx,scale
  ! Particle-based arrays
  real(dp),dimension(1:nvector,1:ndim),save::x
  integer ,dimension(1:nvector,1:twotondim),save::igrid
  real(dp),dimension(1:3)::skip_loc

  open(42,file="Cell_Particles.txt",status="old",position="append")

  ! Mesh spacing in that level
  dx=0.5D0**ilevel
  nx_loc=(icoarse_max-icoarse_min+1)
  skip_loc=(/0.0d0,0.0d0,0.0d0/)
  if(ndim>0)skip_loc(1)=dble(icoarse_min)
  if(ndim>1)skip_loc(2)=dble(jcoarse_min)
  if(ndim>2)skip_loc(3)=dble(kcoarse_min)
  scale=boxlen/dble(nx_loc)

  ! Rescale position at level ilevel
  do idim=1,ndim
     do j=1,np
        x(j,idim)=xp(ind_part(j),idim)/scale+skip_loc(idim)
     end do
  end do
  do idim=1,ndim
     do j=1,np
        x(j,idim)=x(j,idim)-xg(ind_grid(ind_grid_part(j)),idim)
     end do
  end do
  do idim=1,ndim
     do j=1,np
        x(j,idim)=x(j,idim)/dx
     end do
  end do

  do j=1,np
    write(42,'(3f9.4)') x(j,1:ndim)
  enddo
  
  close(42)

end subroutine write_chunk
